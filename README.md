# Simolii Application

[![Build Status](https://img.shields.io/travis/vttn/simolii/master.svg?style=flat-square)](https://travis-ci.org/vttn/simolii)
[![License](https://img.shields.io/packagist/l/vttn/simolii.svg?style=flat-square)](https://packagist.org/packages/vttn/simolii)

An application created with [CakePHP](https://cakephp.org) 3.x.

The source code can be found here: [vttn/simolii](https://github.com/vttn/simolii).
