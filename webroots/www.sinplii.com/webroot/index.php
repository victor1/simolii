<?php
// set environment
if (!defined('APPLICATION_ENV')) {
    if (in_array($_SERVER['SERVER_NAME'], array('sinplii.dev'))){
        define('APPLICATION_ENV', 'development');
    }else {
        define('APPLICATION_ENV', 'production');
    }
}


/**
 * The actual directory name for the application directory. Normally
 * named 'src'.
 */
define('APP_DIR', 'Common');

/**
 * The actual directory name for the application directory. Normally
 * named 'src'.
 */
define('APP_WEBROOT', 'www.sinplii.com');

require dirname(dirname(dirname(__DIR__))) . DIRECTORY_SEPARATOR . 'vendor' . DIRECTORY_SEPARATOR .'autoload.php';

use Common\Http\Application;
use Cake\Http\Server;

// Bind your application to the server.
$server = new Server(new Application(dirname(dirname(dirname(__DIR__))) . DIRECTORY_SEPARATOR . 'Common' . DIRECTORY_SEPARATOR . 'Config'));

// Run the request/response through the application
// and emit the response.
$server->emit($server->run());
