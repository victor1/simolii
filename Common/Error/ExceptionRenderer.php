<?php

namespace Common\Error;
use Cake\Error\ExceptionRenderer as ParentExceptionRenderer;

class ExceptionRenderer extends ParentExceptionRenderer
{
    /**
     * Get an error code value within range 400 to 506
     *
     * @param \Exception $exception Exception.
     * @return int Error code value within range 400 to 506
     */
    protected function _code($exception)
    {
        $code = 500;
        $exception = $this->_unwrap($exception);
        $errorCode = $exception->getCode();
        if (($errorCode >= 400 && $errorCode < 506) || $errorCode == 200) {
            $code = $errorCode;
        }

        return $code;
    }
}