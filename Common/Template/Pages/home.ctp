<?php

$this->layout = false;
$siteName = $this->request->host();

$cakeDescription = 'Welcome to '.$siteName.' | An example of multiple CakePHP install';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="/favicon.ico">

    <title><?=$cakeDescription?></title>

    <!-- Bootstrap core CSS -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-104937363-1', 'auto');
        ga('send', 'pageview');

    </script>
</head>

<body>

<div class="container">
    <div class="header clearfix">
        <h3 class="text-muted"><?=ucfirst($siteName)?></h3>
    </div>

    <div class="jumbotron">

        <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
        <ins class="adsbygoogle" style="display:block" data-ad-client="ca-pub-1429680948604633" data-ad-slot="9058294437" data-ad-format="auto"></ins>
        <script>(adsbygoogle=window.adsbygoogle||[]).push({});</script>
    </div>

    <div class="row marketing">
        <div class="col-lg-12">
            <p>This website is an example of how to develop multiple websites under one CakePHP framework install.</p>
        </div>
        <div class="col-lg-6">

            <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
            <ins class="adsbygoogle" style="display:block" data-ad-client="ca-pub-1429680948604633" data-ad-slot="9058294437" data-ad-format="auto"></ins>
            <script>(adsbygoogle=window.adsbygoogle||[]).push({});</script>
        </div>

        <div class="col-lg-6">

            <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
            <ins class="adsbygoogle" style="display:block" data-ad-client="ca-pub-1429680948604633" data-ad-slot="9058294437" data-ad-format="auto"></ins>
            <script>(adsbygoogle=window.adsbygoogle||[]).push({});</script>
        </div>
    </div>

    <footer class="footer">
        <p>&copy; <?=date('Y')?></p><p><a target="_blank" href="https://github.com/vttn/simolii/commits/master" hreflang="en">Git repository</a></p>
    </footer>
    <!-- Start of StatCounter Code for Default Guide -->
    <script type="text/javascript">
        var sc_project=11425387;
        var sc_invisible=1;
        var sc_security="6aad38fc";
        var scJsHost = (("https:" == document.location.protocol) ?
                        "https://secure." : "http://www.");
        document.write("<sc"+"ript type='text/javascript' src='" +
                       scJsHost+
                       "statcounter.com/counter/counter.js'></"+"script>");
    </script>
    <noscript><div class="statcounter"><a title="web statistics"
                                          href="http://statcounter.com/" target="_blank"><img
                    class="statcounter"
                    src="//c.statcounter.com/11425387/0/6aad38fc/1/" alt="web
statistics"></a></div></noscript>
    <!-- End of StatCounter Code for Default Guide -->

</div> <!-- /container -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</body>
</html>
